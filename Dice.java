import java.util.Random;

public class Dice
{
    public Dice() {}

    public int getValue() { return new Random().nextInt(6) + 1; }
}