import java.awt.*;

public class GUIPlayerInfo extends Canvas
{
    public String Name;
    public int Money;
    public int OwnedPlaces;
    public GUIPlayerInfo(String n)
    {
        Name = n;
    }
    public Rectangle ClipBounds;

    @Override
    public void paint(Graphics g)
    {
        g.clearRect(0,0,100,100);
        update(g);
    }

    @Override
    public void update(Graphics g)
    {
        //System.out.println("Update called");
        //g.setClip(0,0,200,200);
        g.drawLine(0,0,100,0);
        g.drawLine(0,0,0,100);
        g.drawLine(0,100,100,100);
        g.drawLine(100,100,100,0);
        Font f = new Font(Font.SANS_SERIF,Font.PLAIN, 14);
        FontMetrics met = g.getFontMetrics(f);
        g.setFont(f);
        int center = 109/2-Math.round(met.stringWidth(Name+" Stats")/2);
        g.drawString(Name +" Stats", center, 20);
        g.drawString(" $" + Money, 0, 50);
        g.drawString(" Owned: " + OwnedPlaces,0,80);
    }
}